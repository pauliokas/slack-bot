package com.bitbucket.pauliokas.slack.app.rtm;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.core.convert.converter.Converter;

import java.io.IOException;

public class SlackMessageToStringConverter<T extends GenericSlackMessage, R extends String> implements Converter<T, String> {

    private final ObjectMapper objectMapper;

    public SlackMessageToStringConverter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public String convert(T source) {
        return readValue(source);
    }

    @SneakyThrows
    private String readValue(T source) {
        return objectMapper.writeValueAsString(source);
    }
}
