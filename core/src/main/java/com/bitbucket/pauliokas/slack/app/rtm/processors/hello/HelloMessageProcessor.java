package com.bitbucket.pauliokas.slack.app.rtm.processors.hello;

import com.bitbucket.pauliokas.slack.app.rtm.AbstractSlackMessageProcessor;
import com.bitbucket.pauliokas.slack.app.rtm.MessageType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class HelloMessageProcessor extends AbstractSlackMessageProcessor<HelloMessage> {

    public HelloMessageProcessor() {
        super(MessageType.HELLO);
    }

    @Override
    public void handleMessage(HelloMessage message) {
        log.debug("Received hello message");
    }
}
