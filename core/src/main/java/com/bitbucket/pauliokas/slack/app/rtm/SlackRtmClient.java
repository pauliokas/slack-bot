package com.bitbucket.pauliokas.slack.app.rtm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;

public class SlackRtmClient extends Endpoint {

    private static final Logger log = LoggerFactory.getLogger(SlackRtmClient.class);

    private final MessageHandler messageHandler;

    public SlackRtmClient(MessageHandler messageHandler) {
        Assert.notNull(messageHandler, "Parameter 'messageHandler' cannot be null.");
        this.messageHandler = messageHandler;
    }

    @Override
    public void onOpen(Session session, EndpointConfig config) {
        session.addMessageHandler(messageHandler);
    }

    @Override
    public void onError(Session session, Throwable thr) {
        log.error("Error on RTM Slack client", thr);
    }

}
