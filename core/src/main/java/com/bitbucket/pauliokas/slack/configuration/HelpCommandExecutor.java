package com.bitbucket.pauliokas.slack.configuration;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

class HelpCommandExecutor implements CommandExecutor {

    private final List<CommandConfiguration> commands;

    public HelpCommandExecutor(final List<CommandConfiguration> commands) {
        this.commands = commands;
    }

    @Override
    public CommandResult execute(CommandData commandData) {
        if (!commandData.getArguments().isEmpty()) {
            final String specificCommands = commands.stream()
                    .filter(c -> c.getName().equalsIgnoreCase(commandData.getArguments()))
                    .map(toCommandHelp())
                    .collect(Collectors.joining("\n"));

            if (!specificCommands.isEmpty()) {
                return CommandResult.message(specificCommands);
            }
        }

        return CommandResult.message(commands.stream()
                .map(toCommandHelp())
                .collect(Collectors.joining("\n")));
    }

    private Function<CommandConfiguration, String> toCommandHelp() {
        return CommandConfiguration::getName;
    }
}
