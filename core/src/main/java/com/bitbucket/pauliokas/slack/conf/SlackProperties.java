package com.bitbucket.pauliokas.slack.conf;

import lombok.Data;
import lombok.Value;

@Data
public class SlackProperties {

    private String root;
}
