package com.bitbucket.pauliokas.slack.app.slack;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Channel {

    // @formatter:off
    @JsonProperty("id") private String id;
    @JsonProperty("name") private String name;
    @JsonProperty("is_channel") private boolean channel;
    @JsonProperty("is_group") private boolean group;
    @JsonProperty("is_mpim") private boolean mpim;
    @JsonProperty("members") private List<String> members;
    // @formatter:on
}
