package com.bitbucket.pauliokas.slack.app.slack;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class User {

    // @formatter:off
    @JsonProperty("id") private String id;
    @JsonProperty("name") private String name;
    @JsonProperty("deleted") private boolean deleted;
    @JsonProperty("profile") private Profile profile;
    // @formatter:on

    @Data
    public static class Profile {

        // @formatter:off
        @JsonProperty("first_name") private String firstName;
        @JsonProperty("last_name") private String lastName;
        @JsonProperty("real_name") private String realName;
        @JsonProperty("email") private String email;
        @JsonProperty("is_admin") private boolean admin;
        @JsonProperty("is_owner") private boolean owner;
        @JsonProperty("is_primary_owner") private boolean primaryOwner;
        @JsonProperty("is_restricted") private boolean restricted;
        @JsonProperty("is_ultra_restricted") private boolean ultraRestricted;
        // @formatter:on
    }
}
