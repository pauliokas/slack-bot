package com.bitbucket.pauliokas.slack.app.rtm.processors.message;

import com.bitbucket.pauliokas.slack.app.rtm.AbstractSlackMessageProcessor;
import com.bitbucket.pauliokas.slack.app.rtm.MessageType;
import com.bitbucket.pauliokas.slack.app.slack.InstantMessage;
import com.bitbucket.pauliokas.slack.app.slack.SlackServerInformation;
import com.bitbucket.pauliokas.slack.app.web.SlackService;
import com.bitbucket.pauliokas.slack.app.web.WebImList;
import com.bitbucket.pauliokas.slack.configuration.BotConfiguration;
import com.bitbucket.pauliokas.slack.configuration.CommandConfiguration;
import com.bitbucket.pauliokas.slack.configuration.CommandData;
import com.bitbucket.pauliokas.slack.configuration.CommandResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
@Slf4j
public class MessageProcessor extends AbstractSlackMessageProcessor<TextMessage> {

    private static final Pattern MENTION_PATTERN = Pattern.compile("\\s*<@(.*?)>\\S?\\s*");

    @Autowired
    private SlackService slackService;

    @Autowired
    private BotConfiguration botConfiguration;

    @Autowired
    private SlackServerInformation slackServerInformation;

    public MessageProcessor() {
        super(MessageType.MESSAGE);
    }

    @Override
    public void handleMessage(TextMessage message) {
        if (message.getText() == null) {
            return;
        }

        if (message.getUser().equals(slackServerInformation.getCurrentUserId())) {
            return;
        }

        final WebImList instantMessages = slackService.imList();
        final Optional<String> imUser = instantMessages.getIms().stream()
                .map(InstantMessage::getId)
                .filter(u -> u.equals(message.getChannel()))
                .findFirst();

        String possibleCommand;
        if (!imUser.isPresent()) {
            final Matcher matcher = MENTION_PATTERN.matcher(message.getText());
            if (!matcher.find()) {
                return;
            }

            final String mention = matcher.group(1);
            if (!mention.equals(slackServerInformation.getCurrentUserId())) {
                return;
            }

            possibleCommand = message.getText().substring(matcher.end()).trim();
        } else {
            possibleCommand = message.getText();
        }

        final Optional<Map.Entry<CommandConfiguration, Matcher>> matchingCommandEntry = botConfiguration.getCommands().stream()
                .collect(Collectors.toMap(Function.identity(), c -> c.getPattern().matcher(possibleCommand)))
                .entrySet()
                .stream()
                .filter(e -> e.getValue().find())
                .findFirst();

        if (!matchingCommandEntry.isPresent()) {
            return;
        }

        final CommandConfiguration matchingCommand = matchingCommandEntry.get().getKey();
        final Matcher commandMatcher = matchingCommandEntry.get().getValue();

        final String parsedCommand = getGroup(commandMatcher, 1);
        final String parsedArguments = getGroup(commandMatcher, 2);

        final CommandData commandData = new CommandData(message.getText(), message.getUser(), parsedCommand, parsedArguments);

        final CommandResult commandResult = matchingCommand.getCommandExecutor().execute(commandData);

        if (commandResult.getMessage() != null) {
            slackService.chatPostMessage(message.getChannel(), commandResult.getMessage());
        }
    }

    private String getGroup(final Matcher matcher, final int group) {
        String parsedArguments = null;

        if (matcher.groupCount() >= group) {
            parsedArguments = matcher.group(group);
        }

        if (parsedArguments == null) {
            parsedArguments = "";
        }

        return parsedArguments;
    }
}
