package com.bitbucket.pauliokas.slack.configuration;

import lombok.Getter;
import org.springframework.util.Assert;

@Getter
public class SlackConfiguration {

    private final String token;

    SlackConfiguration(String token) {
        Assert.hasText(token, "Parameter 'token' must have text.");

        this.token = token;
    }
}
