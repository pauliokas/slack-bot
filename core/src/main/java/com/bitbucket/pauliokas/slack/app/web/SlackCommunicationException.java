package com.bitbucket.pauliokas.slack.app.web;

public class SlackCommunicationException extends RuntimeException {

    public SlackCommunicationException(String method, String error) {
        super(String.format("Error while calling method '%s': %s", method, error));
    }
}
