package com.bitbucket.pauliokas.slack.configuration;

import lombok.Getter;
import org.springframework.util.Assert;

import java.util.regex.Pattern;

@Getter
public class CommandConfiguration {

    private final String name;
    private final Pattern pattern;
    private final CommandExecutor commandExecutor;

    public CommandConfiguration(final String name, final Pattern pattern, final CommandExecutor commandExecutor) {
        Assert.hasText(name, "Parameter 'name' must have text.");
        Assert.notNull(pattern, "Parameter 'pattern' can not be null.");
        Assert.notNull(commandExecutor, "Parameter 'commandExecutor' can not be null.");

        this.name = name;
        this.pattern = pattern;
        this.commandExecutor = commandExecutor;
    }
}
