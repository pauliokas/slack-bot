package com.bitbucket.pauliokas.slack.conf;

import com.bitbucket.pauliokas.slack.app.rtm.*;
import com.bitbucket.pauliokas.slack.app.rtm.sender.SlackRtmMessageSender;
import com.bitbucket.pauliokas.slack.app.slack.SlackServerInformation;
import com.bitbucket.pauliokas.slack.app.web.SlackService;
import com.bitbucket.pauliokas.slack.app.web.WebRtmStart;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.tyrus.client.ClientManager;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.DeploymentException;
import javax.websocket.MessageHandler;
import javax.websocket.Session;
import java.io.IOException;
import java.net.URI;

@Configuration
@EnableCaching
public class AppConfiguration {

    @Bean
    public RestOperations restOperations() {
        return new RestTemplate();
    }

    @Bean
    public SlackService slackService() {
        return new SlackService();
    }

    @Bean
    public SlackServerInformation slackServerInformation() {
        return new SlackServerInformation();
    }

    @Bean
    @ConfigurationProperties(prefix = "slack")
    public SlackProperties slackProperties() {
        return new SlackProperties();
    }

    @Bean
    public SlackRtmMessageSender slackRtmMessageSender() throws IOException, DeploymentException {
        return new SlackRtmMessageSender(createRtmSession(), webSocketConversionService());
    }

    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapExpiringCacheManager();
    }

    @Bean
    protected GenericConversionService webSocketConversionService() {
        final JsonFactory jsonFactory = new JsonFactory();
        final ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

        final GenericConversionService conversionService = new GenericConversionService();
        conversionService.addConverterFactory(new SlackMessageFromStringConverterFactory(objectMapper));
        conversionService.addConverter(GenericSlackMessage.class, String.class, new SlackMessageToStringConverter<>(objectMapper));

        return conversionService;
    }

    @Bean
    protected MessageHandler messageHandler() {
        return new SlackMessageHandler(webSocketConversionService());
    }

    @Bean
    protected SlackRtmClient slackRtmEndpoint() {
        return new SlackRtmClient(messageHandler());
    }

    private Session createRtmSession() throws DeploymentException, IOException {
        final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().build();
        final ClientManager client = ClientManager.createClient();

        final WebRtmStart rtmStartResponse = slackService().rtmStart();
        return client.connectToServer(slackRtmEndpoint(), cec, URI.create(rtmStartResponse.getUrl()));
    }
}
