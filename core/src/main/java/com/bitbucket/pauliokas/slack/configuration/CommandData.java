package com.bitbucket.pauliokas.slack.configuration;

import lombok.Value;

@Value
public class CommandData {

    private String message;
    private String user;
    private String command;
    private String arguments;
}
