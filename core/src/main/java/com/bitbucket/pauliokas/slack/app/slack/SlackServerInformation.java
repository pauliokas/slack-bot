package com.bitbucket.pauliokas.slack.app.slack;

import com.bitbucket.pauliokas.slack.app.web.SlackService;
import com.bitbucket.pauliokas.slack.app.web.WebAuthTest;
import org.springframework.beans.factory.annotation.Autowired;

public class SlackServerInformation {

    @Autowired
    private SlackService slackService;

    public String getCurrentUserId() {
        final WebAuthTest authTestResponse = slackService.authTest();
        return authTestResponse.getUserId();
    }
}
