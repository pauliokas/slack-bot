package com.bitbucket.pauliokas.slack.conf;

import com.google.common.cache.CacheBuilder;
import org.springframework.cache.Cache;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

class ConcurrentMapExpiringCacheManager extends ConcurrentMapCacheManager {

    @Override
    protected Cache createConcurrentMapCache(String name) {
        return new ConcurrentMapCache(name, buildCacheMap(), isAllowNullValues());
    }

    private ConcurrentMap<Object, Object> buildCacheMap() {
        return CacheBuilder.newBuilder()
                .expireAfterAccess(15, TimeUnit.SECONDS)
                .build()
                .asMap();
    }
}
