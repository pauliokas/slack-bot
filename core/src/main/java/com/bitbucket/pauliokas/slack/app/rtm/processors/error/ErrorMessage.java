package com.bitbucket.pauliokas.slack.app.rtm.processors.error;

import com.bitbucket.pauliokas.slack.app.rtm.GenericSlackMessage;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ErrorMessage extends GenericSlackMessage {

    private SlackError error;
}
