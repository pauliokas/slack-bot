package com.bitbucket.pauliokas.slack.app.rtm;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;

import javax.websocket.MessageHandler;
import java.util.List;

@Slf4j
public class SlackMessageHandler implements MessageHandler.Whole<String> {

    private final ConversionService conversionService;

    @Autowired
    private List<SlackMessageProcessor<?>> messageProcessors;

    public SlackMessageHandler(ConversionService conversionService) {
        if (!conversionService.canConvert(String.class, GenericSlackMessage.class)) {
            throw new IllegalArgumentException("Conversion service must be able to convert String to GenericSlackMessage");
        }
        this.conversionService = conversionService;
    }

    @Override
    public void onMessage(String message) {
        if (log.isTraceEnabled()) {
            log.trace("Received message on Slack RTM: " + message);
        }

        final GenericSlackMessage abstractMessage = conversionService.convert(message, GenericSlackMessage.class);

        for (SlackMessageProcessor messageProcessor : messageProcessors) {
            if (messageProcessor.canHandle(abstractMessage.getType())) {
                final Object messageObject = conversionService.convert(message, messageProcessor.getMessageType());

                //noinspection unchecked
                messageProcessor.handleMessage((GenericSlackMessage) messageObject);
            }
        }
    }
}
