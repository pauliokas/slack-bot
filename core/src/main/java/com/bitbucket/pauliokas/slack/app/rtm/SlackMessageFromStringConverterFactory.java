package com.bitbucket.pauliokas.slack.app.rtm;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;

public class SlackMessageFromStringConverterFactory implements ConverterFactory<String, GenericSlackMessage> {

    private final ObjectMapper objectMapper;

    public SlackMessageFromStringConverterFactory(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public <T extends GenericSlackMessage> Converter<String, T> getConverter(Class<T> targetType) {
        return new SlackMessageFromStringConverter<>(objectMapper, targetType);
    }
}
