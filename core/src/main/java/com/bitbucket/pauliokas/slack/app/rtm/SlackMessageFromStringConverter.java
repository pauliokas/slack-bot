package com.bitbucket.pauliokas.slack.app.rtm;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.core.convert.converter.Converter;

import java.io.IOException;

public class SlackMessageFromStringConverter<T extends GenericSlackMessage> implements Converter<String, T> {

    private final ObjectMapper objectMapper;
    private final Class<T> targetType;

    public SlackMessageFromStringConverter(ObjectMapper objectMapper, Class<T> targetType) {
        this.objectMapper = objectMapper;
        this.targetType = targetType;
    }

    @Override
    public T convert(String source) {
        return readValue(source);
    }

    @SneakyThrows
    private T readValue(String source) {
        return objectMapper.readValue(source, targetType);
    }
}
