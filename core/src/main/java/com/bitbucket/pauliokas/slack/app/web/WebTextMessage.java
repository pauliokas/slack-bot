package com.bitbucket.pauliokas.slack.app.web;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class WebTextMessage extends BaseWebResponse {

    private TextMessageAsdf message;

    @Data
    private class TextMessageAsdf {
        private String ts;
    }
}
