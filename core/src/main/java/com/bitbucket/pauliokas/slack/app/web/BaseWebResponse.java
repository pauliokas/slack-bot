package com.bitbucket.pauliokas.slack.app.web;

import lombok.Data;

@Data
public class BaseWebResponse {

    private boolean ok;
    private String error;
    private String warning;
}
