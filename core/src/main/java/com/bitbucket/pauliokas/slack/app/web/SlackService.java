package com.bitbucket.pauliokas.slack.app.web;

import com.bitbucket.pauliokas.slack.conf.SlackProperties;
import com.bitbucket.pauliokas.slack.configuration.BotConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Map;

@Slf4j
public class SlackService {

    @Autowired
    private SlackProperties slackProperties;

    @Autowired
    private BotConfiguration botConfiguration;

    @Autowired
    private RestOperations restOperations;

    @Cacheable("slack[rtm.start]")
    public WebRtmStart rtmStart() {
        return getResponse("rtm.start", variablesMap(), WebRtmStart.class);
    }

    @Cacheable("slack[auth.test]")
    public WebAuthTest authTest() {
        return getResponse("auth.test", variablesMap(), WebAuthTest.class);
    }

    public WebTextMessage chatPostMessage(String channel, String message) {
        return getResponse("chat.postMessage", variablesMap(
                "channel", channel,
                "text", message,
                "as_user", Boolean.TRUE.toString()
        ), WebTextMessage.class);
    }

    @Cacheable("slack[im.list]")
    public WebImList imList() {
        return getResponse("im.list", variablesMap(), WebImList.class);
    }

    private MultiValueMap<String, String> variablesMap(String... args) {
        MultiValueMap<String, String> variablesMap = new LinkedMultiValueMap<>();
        for (int i = 0; i < args.length; i += 2) {
            variablesMap.add(args[i], args[i + 1]);
        }

        return variablesMap;
    }

    private <T extends BaseWebResponse> T getResponse(String method, MultiValueMap<String, String> arguments, Class<T> responseClass) {
        log.debug("Executing Slack Web API method: " + method);

        final T slackResponse = doRequest(method, arguments, responseClass);

        validate(method, slackResponse);

        return slackResponse;
    }

    private <T extends BaseWebResponse> T doRequest(String method, MultiValueMap<String, String> arguments, Class<T> responseClass) {
        final UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromHttpUrl(slackProperties.getRoot() + "/api/" + method);
        uriComponentsBuilder.queryParam("token", botConfiguration.getSlack().getToken());
        for (Map.Entry<String, List<String>> entry : arguments.entrySet()) {
            uriComponentsBuilder.queryParam(entry.getKey(), entry.getValue().toArray());
        }

        final RequestEntity<T> requestEntity = new RequestEntity<>(HttpMethod.GET, uriComponentsBuilder.build().toUri());
        final ResponseEntity<T> responseEntity = restOperations.exchange(requestEntity, responseClass);

        return responseEntity.getBody();
    }

    private <T extends BaseWebResponse> void validate(String method, T slackResponse) {
        if (slackResponse.getWarning() != null) {
            log.warn(String.format("Warning from Slack Web API while executing %s: %s", method, slackResponse.getWarning()));
        }

        if (slackResponse.getError() != null) {
            log.error(String.format("Error from Slack Web API while executing %s: %s", method, slackResponse.getError()));
        }

        if (!slackResponse.isOk()) {
            throw new SlackCommunicationException(method, slackResponse.getError());
        }
    }
}
