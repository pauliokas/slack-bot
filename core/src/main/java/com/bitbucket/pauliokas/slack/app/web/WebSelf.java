package com.bitbucket.pauliokas.slack.app.web;

import lombok.Data;

@Data
public class WebSelf {

    private String id;
    private String name;
}
