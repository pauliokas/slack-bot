package com.bitbucket.pauliokas.slack.configuration;

public interface CommandExecutor {

    CommandResult execute(CommandData commandData);
}
