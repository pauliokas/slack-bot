package com.bitbucket.pauliokas.slack.app.rtm;

import org.springframework.core.GenericTypeResolver;

public abstract class AbstractSlackMessageProcessor<T extends GenericSlackMessage> implements SlackMessageProcessor<T> {

    private final MessageType supportedType;

    protected AbstractSlackMessageProcessor(MessageType supportedType) {
        this.supportedType = supportedType;
    }

    @Override
    public Class<T> getMessageType() {
        return resolveTypeArgument();
    }

    @Override
    public boolean canHandle(MessageType messageType) {
        return supportedType.equals(messageType);
    }

    private Class<T> resolveTypeArgument() {
        Class<?>[] resolved = GenericTypeResolver.resolveTypeArguments(getClass(), SlackMessageProcessor.class);

        //noinspection unchecked
        return (Class<T>) resolved[0];
    }
}
