package com.bitbucket.pauliokas.slack.app.web;

import com.bitbucket.pauliokas.slack.app.slack.InstantMessage;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class WebImList extends BaseWebResponse {

    // @formatter:off
    @JsonProperty("ims") private List<InstantMessage> ims;
    // @formatter:on
}
