package com.bitbucket.pauliokas.slack.app.web;

import com.bitbucket.pauliokas.slack.app.slack.Channel;
import com.bitbucket.pauliokas.slack.app.slack.InstantMessage;
import com.bitbucket.pauliokas.slack.app.slack.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class WebRtmStart extends BaseWebResponse {

    @JsonProperty("url")
    private String url;
    @JsonProperty("self")
    private User self;
    @JsonProperty("channels")
    private List<Channel> channels;
    @JsonProperty("groups")
    private List<Channel> groups;
    @JsonProperty("ims")
    private List<InstantMessage> ims;

}
