package com.bitbucket.pauliokas.slack.app.rtm.sender;

import com.bitbucket.pauliokas.slack.app.rtm.GenericSlackMessage;
import lombok.SneakyThrows;
import org.springframework.core.convert.ConversionService;
import org.springframework.util.Assert;

import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;

public class SlackRtmMessageSender {

    private final Session session;
    private final ConversionService conversionService;

    public SlackRtmMessageSender(Session session, ConversionService conversionService) {
        Assert.notNull(session, "Parameter 'session' cannot be null.");
        Assert.notNull(conversionService, "Parameter 'conversionService' cannot be null.");
        Assert.isTrue(conversionService.canConvert(GenericSlackMessage.class, String.class), "ConversionService should be able to convert message to String.");

        this.session = session;
        this.conversionService = conversionService;
    }

    @SneakyThrows
    public void send(GenericSlackMessage message) {
        final RemoteEndpoint.Basic remote = session.getBasicRemote();
        remote.sendText(conversionService.convert(message, String.class));
    }
}
