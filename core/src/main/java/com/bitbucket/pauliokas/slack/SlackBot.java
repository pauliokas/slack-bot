package com.bitbucket.pauliokas.slack;

import com.bitbucket.pauliokas.slack.app.AppPackageMarker;
import com.bitbucket.pauliokas.slack.conf.AppConfiguration;
import com.bitbucket.pauliokas.slack.configuration.BotConfiguration;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;

import java.util.concurrent.CountDownLatch;

@SpringBootApplication(scanBasePackageClasses = AppPackageMarker.class)
@Import(AppConfiguration.class)
public class SlackBot {

    public static void run(final BotConfiguration properties) throws InterruptedException {
        validate(properties);

        new SpringApplicationBuilder(SlackBot.class)
                .logStartupInfo(false)
                .addCommandLineProperties(false)
                .bannerMode(Banner.Mode.OFF)
                .initializers(new ApplicationInitializer(properties))
                .build()
                .run();

        CountDownLatch countDownLatch = new CountDownLatch(1);
        Runtime.getRuntime().addShutdownHook(new Thread(countDownLatch::countDown));

        countDownLatch.await();
    }

    private static void validate(BotConfiguration properties) {
        if (properties == null) {
            throw new IllegalArgumentException("Properties must be specified.");
        }

        if (properties.getSlack() == null) {
            throw new IllegalArgumentException("Slack configuration must be specified.");
        }

        if (properties.getSlack().getToken() == null) {
            throw new IllegalArgumentException("Slack Bot token must be specified.");
        }
    }
}
