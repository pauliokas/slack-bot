package com.bitbucket.pauliokas.slack.configuration;

import org.springframework.util.Assert;

public class CommandResult {

    private final String message;

    private CommandResult() {
        message = null;
    }

    private CommandResult(final String message) {
        Assert.hasText(message, "Parameter 'message' must have text.");
        this.message = message;
    }

    public static CommandResult empty() {
        return new CommandResult();
    }

    public static CommandResult message(final String message) {
        return new CommandResult(message);
    }

    public String getMessage() {
        return message;
    }
}
