package com.bitbucket.pauliokas.slack.configuration;

import lombok.Getter;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

@Getter
public class BotConfiguration {

    private final SlackConfiguration slack;
    private final List<CommandConfiguration> commands;

    private BotConfiguration(final SlackConfiguration slack, List<CommandConfiguration> commands) {
        Assert.notNull(slack);

        this.slack = slack;
        this.commands = commands;
    }

    public static BotConfigurationBuilder builder() {
        return new BotConfigurationBuilder();
    }

    public static class BotConfigurationBuilder {
        private SlackConfiguration slack;
        private boolean help;
        private List<CommandConfiguration> commands = Collections.emptyList();

        private BotConfigurationBuilder() {
        }

        public BotConfiguration build() {
            if (help) {
                List<CommandConfiguration> commands = new LinkedList<>(this.commands);
                final CommandExecutor commandExecutor = new HelpCommandExecutor(commands);
                final CommandConfiguration helpCommand = new CommandConfiguration("help", Pattern.compile("(help)(?:\\s+(.*))?"), commandExecutor);
                commands.add(0, helpCommand);

                this.commands = Collections.unmodifiableList(commands);
            }
            return new BotConfiguration(slack, commands);
        }

        public BotConfigurationBuilder enableHelp() {
            help = true;
            return this;
        }

        public SlackConfigurationBuilder slack() {
            return new SlackConfigurationBuilder();
        }

        public CommandsBuilder commands() {
            return new CommandsBuilder();
        }

        public class SlackConfigurationBuilder {
            private String token;

            private SlackConfigurationBuilder() {
            }

            public BotConfigurationBuilder and() {
                BotConfigurationBuilder.this.slack = new SlackConfiguration(token);
                return BotConfigurationBuilder.this;
            }

            public SlackConfigurationBuilder token(String token) {
                this.token = token;
                return this;
            }
        }

        public class CommandsBuilder {
            private List<CommandConfiguration> commands = new LinkedList<>();

            private CommandsBuilder() {}

            public BotConfigurationBuilder and() {
                BotConfigurationBuilder.this.commands = Collections.unmodifiableList(commands);
                return BotConfigurationBuilder.this;
            }

            public CommandBuilder command(String name) {
                return new CommandBuilder(name);
            }

            public class CommandBuilder {
                private String name;
                private Pattern pattern;
                private CommandExecutor commandExecutor;

                private CommandBuilder(String name) {
                    this.name = name;
                }

                public CommandsBuilder and() {
                    CommandsBuilder.this.commands.add(new CommandConfiguration(name, pattern, commandExecutor));
                    return CommandsBuilder.this;
                }

                public CommandBuilder pattern(Pattern pattern) {
                    this.pattern = pattern;
                    return this;
                }

                public CommandBuilder commandExecutor(CommandExecutor commandExecutor) {
                    this.commandExecutor = commandExecutor;
                    return this;
                }
            }
        }

    }

}
