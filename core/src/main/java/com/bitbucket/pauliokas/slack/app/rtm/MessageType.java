package com.bitbucket.pauliokas.slack.app.rtm;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum MessageType {
    ERROR("error"),
    HELLO("hello"),
    MESSAGE("message"),

    UNKNOWN(null);

    private final String type;

    MessageType(String type) {
        this.type = type;
    }

    @JsonCreator
    public static MessageType forType(String type) {
        return Arrays.stream(MessageType.values())
                .filter(v -> v.type != null && v.type.equals(type))
                .findFirst()
                .orElse(UNKNOWN);
    }

    @JsonValue
    public String getType() {
        return type;
    }
}
