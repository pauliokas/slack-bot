package com.bitbucket.pauliokas.slack.app.slack;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InstantMessage {

    // @formatter:off
    @JsonProperty("id") private String id;
    @JsonProperty("is_im") private boolean im;
    @JsonProperty("user") private String user;
    // @formatter:on
}
