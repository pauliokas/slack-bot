package com.bitbucket.pauliokas.slack.app.web;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class WebAuthTest extends BaseWebResponse {

    @JsonProperty("user")
    private String user;
    @JsonProperty("user_id")
    private String userId;
}
