package com.bitbucket.pauliokas.slack.app.rtm.processors.error;

import lombok.Data;

@Data
public class SlackError {

    private String msg;
    private int code;
}
