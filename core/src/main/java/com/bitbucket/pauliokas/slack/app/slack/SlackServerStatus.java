package com.bitbucket.pauliokas.slack.app.slack;

import lombok.Data;

@Data
public class SlackServerStatus {

    private String id;
    private String name;
}
