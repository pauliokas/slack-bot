package com.bitbucket.pauliokas.slack.app.rtm;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class GenericSlackMessage {

    private MessageType type;
}
