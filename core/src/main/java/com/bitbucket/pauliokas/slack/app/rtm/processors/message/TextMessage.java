package com.bitbucket.pauliokas.slack.app.rtm.processors.message;

import com.bitbucket.pauliokas.slack.app.rtm.GenericSlackMessage;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class TextMessage extends GenericSlackMessage {

    private String text;
    private String channel;
    private String user;
}
