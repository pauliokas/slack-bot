package com.bitbucket.pauliokas.slack;

import com.bitbucket.pauliokas.slack.configuration.BotConfiguration;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

public class ApplicationInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private final BotConfiguration botConfiguration;

    public ApplicationInitializer(BotConfiguration botConfiguration) {
        this.botConfiguration = botConfiguration;
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        final ConfigurableListableBeanFactory beanFactory = applicationContext.getBeanFactory();
        beanFactory.registerSingleton("botConfiguration", botConfiguration);
    }
}
