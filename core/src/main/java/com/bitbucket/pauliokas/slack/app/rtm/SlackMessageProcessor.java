package com.bitbucket.pauliokas.slack.app.rtm;

public interface SlackMessageProcessor<T extends GenericSlackMessage> {

    void handleMessage(T message);

    Class<T> getMessageType();

    boolean canHandle(MessageType messageType);
}
