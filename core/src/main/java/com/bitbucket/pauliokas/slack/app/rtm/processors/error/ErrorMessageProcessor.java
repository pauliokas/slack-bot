package com.bitbucket.pauliokas.slack.app.rtm.processors.error;

import com.bitbucket.pauliokas.slack.app.rtm.AbstractSlackMessageProcessor;
import com.bitbucket.pauliokas.slack.app.rtm.MessageType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ErrorMessageProcessor extends AbstractSlackMessageProcessor<ErrorMessage> {

    public ErrorMessageProcessor() {
        super(MessageType.ERROR);
    }

    @Override
    public void handleMessage(ErrorMessage message) {
        final SlackError error = message.getError();
        log.error("Error on Slack RTM. Message: '{}', code: {}", error.getMsg(), error.getCode());
    }
}
