package com.bitbucket.pauliokas.slack.example;

import com.bitbucket.pauliokas.slack.SlackBot;
import com.bitbucket.pauliokas.slack.configuration.BotConfiguration;
import com.bitbucket.pauliokas.slack.configuration.CommandResult;

import java.util.regex.Pattern;

public class Application {

    public static void main(String... args) throws InterruptedException {
        // @formatter:off
        SlackBot.run(BotConfiguration.builder()
                .slack()
                        .token(args[0])
                        .and()
                .commands()
                        .command("echo")
                                .pattern(Pattern.compile("(echo)(?:\\s*(.*))$"))
                                .commandExecutor(commandData -> CommandResult.message("Echo: " + commandData.getArguments()))
                                .and()
                        .and()
                .enableHelp()
                .build());
        // @formatter:on
    }
}
